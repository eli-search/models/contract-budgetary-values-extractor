# This is the file that implements a flask server to do inferences. It's the file that you will
#  modify to implement the scoring for your own algorithm.

from __future__ import print_function

import json
import os

import flask

from . import predict
from werkzeug.utils import secure_filename
from tika import parser

UPLOAD_FOLDER = '/tmp'
ALLOWED_EXTENSIONS = {'txt', 'pdf', 'docx'}
app = flask.Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route('/ping', methods=['GET'])
def ping():
    """Determine if the container is working and healthy"""
    return flask.Response(response='\n', status=200, mimetype='application/json')


@app.route('/invocations', methods=['POST'])
def transformation():
    print(f"Request received: {flask.request.content_type}")

    data = dict()
    if flask.request.content_type == 'application/json':
        data = flask.request.get_json()
        print(f"Extracted data from JSON input: {data}")
    elif 'multipart/form-data' in flask.request.content_type:
        file = flask.request.files['file']
        print(f"Filename received: {file.filename}")
        if file.filename == '':
            return flask.Response(
                response=json.dumps({'message': 'No file found in multipart/form-data request'}),
                status=400,
                mimetype='application/json'
            )

        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            path = os.path.join(app.config['UPLOAD_FOLDER'], filename)
            print(f"File will be saved in {str(path)}")
            file.save(path)
            print(f"Parsing file content")
            parsed = parser.from_file(path)
            content = parsed['content']
            print(f"Extracted content: {content}")
            data = {'text': content}
            print(f"Removing temporary file at {str(path)}")
            os.remove(path)
    else:
        return flask.Response(
            response=json.dumps({'message': 'This predictor only supports JSON and multipart/form-data data'}),
            status=415,
            mimetype='application/json'
        )

    result = predict.predict(data)
    print(f"Received result from the model: {result}")

    return flask.Response(response=json.dumps(result), status=200, mimetype='application/json')
